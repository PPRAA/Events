# Events

Scripts and checklists for PPRAA official events:

* [Thursday night 2 meter nets](https://codeberg.org/PPRAA/Events/src/branch/main/2MeterNet)
* [Radio Direction Finding (RDF) fox hunts](https://codeberg.org/PPRAA/Events/src/branch/main/RDFFoxHunt)