# 2020-12 RDF Fox Hunt Social Net Script

## Prior to fox hunt (1130 and 1145)

This is __[call sign]__. At twelve noon, the Pikes Peak Radio Amateur Association will be hosting a holiday fox hunt, coordinated on this repeater. All amateur radio operators, and even unlicensed radio listeners, are invited to join this radio direction finding activity. This repeater will be used to give information about the fox hunt, and licensed operators may use this repeater to socialize and exchange RDF tips during the fox hunt. Prizes will be awarded to fox hunt finishers after the hunt ends, at 2pm. For more information, visit the PPRAA website, at PPRAA.org, and join our fox hunt net at twelve noon. Until then, this is __[call sign]__, monitoring.

## Start time (1200)

Calling all radio amateurs! This is the Pikes Peak Radio Amateur Association holiday fox hunt social net. Your net control for today is __[call sign]__. My name is __[name]__. The fox hunt will begin in just a few minutes, after we've listed the rules and checked in our participants.

The purpose of this net is for fox hunt participants to socialize, to exchange tips on radio direction finding, and for the facilitators to announce the prize winners at the end. We would like to thank the Cheyenne Mountain Repeater Group for the use of this 147.345 MHz repeater. To check into this net, you will need to use a *positive* 600 kHz offset, and a 107.2 Hz tone.

Pausing for reset.

Here are the rules of the fox hunt: the fox is a vehicle parked in a publicly-accessible location inside the city limits of Colorado Springs. It will be staionary; it will not move. It will begin transmitting on 146.565 MHz, FM, with v ertical polarization, as soon as net check-ins are complete. Again, that's 146.565 MHz, FM, vertical polarization, and that's a simplex channel commonly used for fox hunts nationwide. The fox will trnasmit for 60 seconds, a t five-minute intervals, so listenj for it at fifteen after, 20 after, 25 after, etc. These transmissions will continue until 1400, or 2pm local time.

This is __[call sign]__, pausing for reset.

Who may participate? If you are a licensed amateur radio operator, please check into this net with your call sign and name; all licensees are welcome to join. If anyone is not licensed, you may also participate, but you may not transmit on this net. Please send your name and email address to __[email address]__, and you will be included in the participants' list and be eligible for prizes.

You are encouraged to use any and all techniques for radio direction finding, to locate the fox vehicle. We are practitioners and advancers of the radio art and science, so you are encouraged to share your techniques on this net. Please observe sound social distancing practices and drive safely as you search for the fox.

Since there are prizes at stake, we ask that those who find the fox should *not* provide hints to other participants. Please give a fair chance to all seekers.

This is __[call sign]__, pausing for reset.

When you find the fox vehicle, please report your name and call sign to the driver of the vehicle. If you would like to compete for youngest or oldest ham to find the fox, please include your date of birth. If you would like to compete for most recently-licensed or longest-licensed ham to find the fox, please include the date of your first license. If you do not have a ham radio license, give your name and email address. If you are wearing a mask, you can talk to the driver in the vehicle. You may write your information on paper and hand it in through the window. Or you may write your information large on a piece of paper, stand back from the vehicle, and show it to the driver. We are on a hill, so you may enjoy the scenery of our beautiful city once you have found the fox, but we ask that you do not loiter at the window once you have submitted your information, so we can follow sound social distancing practices.

This is __[call sign]__, pausing for reset.

I will repeat the key details, in case you need to write any of these down. The fox frequency is 146.565 megahertz, fm, vertical polarization. When you find it, please submit your call sign or email address, your name, your date of birth if you want to compete for that recognition, and the date of your first license if you want to compete for that recognition.

At 2 pm, the fox transmitter will cease transmission, and we will check over our lists of finders to prepare the prizes. We will announce the winners of the prizes here, on this net, shortly after 2pm.

PPRAA has generously offered up what I think are some top-quality prizes, from which the winners will be able to choose. I will describe these prizes in detail in the net at 2pm, for the prize-winners to choose.

This is __[call sign]__, pausing for reset.

Let's proceed with participant check-ins. Anyone who does not have an amateur radio license, please send your name, email address, and birthdate if you want, to me at __[email address]__. For those of you with an amateur radio license, we wi
ll take check-ins by call sign suffix. This is the part of your call sign after the number. For example, my call sign is __[call sign]__, so my call sign suffix begins with __[call sign suffix]__.

Again, please check in with your call sign and your name. At this time, call sign suffixes starting with Alpha through Foxtrot, Alpha through Foxtrot, please check in now.

[Acknowledge all]

At this time, call sign suffixes starting with Golf through Mike, Golf through Mike, please check in now.

[Acknowledge all]

At this time, call sign suffixes starting with November through Sierra, November through Sierra, please check in now.

[Acknowledge all]

At this time, call sign suffixes Tango through Zulu, Tango through Zulu, please check in now.

[Acknowledge all]

Thank you to all who have checked in. We will now activate the fox's transmission on 146.565 Megahertz, and transmit again for 60 seconds, at every five-minute interval after the hour. So listen for it at 12:15, 12:20, 12:25, and so on, 'til 1400, or 2 pm. Good luck everyone, and we hope to see you at the fox location soon! This is __[callsign]__ as net control.

## During the fox hunt

This is the PPRAA radio direction finding fox hunt socializing net, with __[call sign]__ as net control. Participants in the fox hunt are invited to share direction-finding tips and socialize while hunting for the fox.

The fox is transmitting every five minutes on 146.565 megahertz. We thank the Pikes Peak Radio Amateur Association for hosting this activity, and we thank the Cheyenne Mountain Repeater Group for the use of this repeater. Are there any additional checkins for the net at this time?

[Acknowledge checkins]