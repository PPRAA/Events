# 2021 Special Event Station W0C
This special event station is to celebrate the 150th anniversary (sesquicentennial) of the founding of Colorado Springs. We will mark the occasion by operating 1x1 callsign "W0C" from July 23 (a Friday) through July 31 (a Saturday), culminating at the COS150 celebration downtown.

At the downtown festival, we can showcase our special event station accomplishments by running a PPRAA booth, and displaying a map on which our contacts of the preceding week can be flagged.

## Get involved
Here are the steps to participate in operating under our special-event call sign:

### Pre-event setup
1. Join the live chat.
    1. We will use the "Matrix" chat service for real-time communications. Install a phone/tablet app called "Element," or download the Element Matrix client from [https://element.io](https://element.io).
    2. Set up a new Matrix account with your callsign as the name, on the matrix.org server.
    3. Log into matrix and join the #W0C channel. (That's "Whisky Zero Charlie,"  our 1x1 call sign, just in case your browser didn't make it clear there's a zero in the middle.)
    4. Say hi to your fellow PPRAA members.
    5. Ask LD (W0XLD) for a CloudLog account.
2. Access the web-based logging application.
    1. Access the logging website at [https://w0xld.com/cloudlog/](https://w0xld.com/cloudlog/).
    2. Log in with the username and password provided by W0XLD.
    3. Try creating some fake QSO records, and make sure you can access the QSO records others have created.

### During the event
The only valid dates for working with the W0C call sign are July 23 - August 1. Do not use this callsign outside of those dates.
1. Coordinate with other operators.
    1. Open your Matrix chat and ask the other operators in #W0C if anyone is working the band/mode you want to work.
    2. If there's already someone working the band/mode you're interested in, coordinate with the other operators in the channel to identify a different band or different mode you can work instead.
    3. Announce in the Matrix channel when you are ready to begin operations, confirming band and mode.
2. Have fun making contacts!
    1. Remember that the purpose of this special event is to commemorate a special occasion, so while you are calling CQ, announce the purpose of our special event. You may  say something like:
        > CQ special event, CQ special event station Whisky Zero Charlie. The Pikes Peak Radio Amateur Association is celebrating the 150th anniversary of the founding of Colorado Springs. This is special event station Whisky Zero Charlie calling CQ and listening.
3. Log your contacts immediately.
    (Adding steps to operate CloudLog)