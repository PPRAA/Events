# Festival Resources

Handouts and activity documentation for booths or display tents at fairs, festivals, PPREX events, hamfests, etc.

## Calendar
Upcoming events:

* 2021-07-31: COS 150 (Colorado Springs sesquicentennial festival downtown)
* 2021-08-14: Mobile amateur radio station car show
* 2021-10-02: Double Fox Hunt

## Handouts

* Why Amateur Radio? / How to Get Started: [Source](https://codeberg.org/PPRAA/Events/src/branch/main/Festivals/why-amateur-radio.odt)/[PDF](https://codeberg.org/PPRAA/Events/src/branch/main/Festivals/why-amateur-radio.pdf)
* Pikes Peak Radio Amateur Association (PPRAA): [Source](https://codeberg.org/PPRAA/Events/src/branch/main/Festivals/ppraa-brochure.svg)/[PDF](https://codeberg.org/PPRAA/Events/src/branch/main/Festivals/ppraa-brochure.pdf)

## Science day at the WMMI

This was held on Saturday, July 10. Booth volunteers were: John, Jason, LD, Derek B, RL, Joe, Kyle, Don. Thanks to everyone who pitched in!

## COS 150

Timeline:
* Special event callsign ops: Friday, July 23 - Saturday, July 31.
* Walkthrough: Wednesday, July 28, 5:00 pm. Meet at Pioneer's Museum front entrance.
* Festival booth: July 31
    * 8:00 AM - 11:00 AM: Vendor setup
    * 11:00 AM - 12:00 PM: Parade
    * 12:00 PM - 8:00 PM: Festival
    * 8:00 PM - 8:30 PM: Tear-downtown

Volunteers:
* Jason: canopy, banner, table, IC-7300, laptop, solar, battery
* John: table, chairs, go-kit, laptop, PIO display, handouts, solar, battery
* LD: laptop, TV, handouts, solar, battery, inverter
* Earl: Arrow antenna, HT, laptop for satellite demos
* Don: CW paddles
* Derek: Charisma, good looks, ambassadorial attitude, MaD OpeRaToR SkiLLz
* Kyle: Charisma, good looks, ambassadorial attitude, MaD OpeRaToR SkiLLz

Todo:
* Identify volunteer for walkthrough on the 28th
* Once we understand the space, decide whether to deploy an HF antenna, and what kind